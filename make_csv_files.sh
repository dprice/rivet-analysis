#!/bin/bash
FILES=($(ls -lht log* | head -n22 | awk '{ print $NF}'))

fname=$(echo $i | cut -d'.' -f 2)
for i in "${FILES[@]}"
do
  fname=$(echo $i | cut -d'.' -f 2)
  echo $fname
  grep data $i > $fname.csv 
  sed  's/Rivet.Analysis.myRivetRoutine_EL: INFO  data: //g' $fname.csv &
  sed  's/Rivet.Analysis.myRivetRoutine_MU: INFO  data: //g' $fname.csv &
done
