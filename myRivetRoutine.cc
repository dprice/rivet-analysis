// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/WFinder.hh"
#include "Rivet/Math/LorentzTrans.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include <iostream>
#include <fstream>

using namespace std;

ofstream myfile;

namespace Rivet {

  /// @brief w+gamma+jets studies
  class myRivetRoutine: public Analysis {
    public:

      /// Constructor
      myRivetRoutine(const string name = "myRivetRoutine", size_t channel = 0,
        const string ref_data = "myRivetRoutine"): Analysis(name) {
        _mode = channel; // pick electron channel by default (muon == 1, electron == 0)
      }

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      // Prompt photons
      const Cut photoncut = Cuts::abspid == PID::PHOTON && Cuts::pT > 15 * GeV && Cuts::abseta < 2.37;
      PromptFinalState photon_fs(photoncut);
      declare(photon_fs, "Photons");

      // Prompt leptons
      const PromptFinalState barelepton_fs = _mode ? Cuts::abspid == PID::MUON : Cuts::abspid == PID::ELECTRON;
      // Dressed leptons [should we do this??]
      const IdentifiedFinalState allphoton_fs(PID::PHOTON); // photons used for lepton dressing
      const Cut leptoncut = Cuts::pT > 5 * GeV && Cuts::abseta < 2.47;
      // use *all* photons for lepton dressing
      const DressedLeptons dressedlepton_fs(allphoton_fs, barelepton_fs, 0.1, leptoncut, true);
      declare(dressedlepton_fs, "Leptons");

      // MET - from https://rivet.hepforge.org/analyses/ATLAS_2017_I1517194.html
      MissingMomentum missmom(FinalState(Cuts::eta < 5.0));
      addProjection(missmom, "mm"); 

      // Jets
      VetoedFinalState jet_fs;
      jet_fs.vetoNeutrinos();
      jet_fs.addVetoPairId(PID::MUON);
      const FastJets fastjets(jet_fs, FastJets::ANTIKT, 0.4);
      declare(fastjets, "Jets");
      Cut cuts = Cuts::abseta < 2.5 && Cuts::pT >= 20 * GeV;

      FinalState fs;

      auto lepton_def = _mode ? PID::ELECTRON : PID::MUON;
      WFinder w_finder(fs, cuts, lepton_def, 40 * GeV, MAXDOUBLE, 0.0 * GeV, 0.0, WFinder::CLUSTERNODECAY,
        WFinder::NOTRACK, WFinder::TRANSMASS);
      declare(w_finder, "W_FINDER");

      m_W_events = 0;

      //initialise the histograms we want to save into the yoda file.
      _h["MET"] = bookHisto1D("MET", 100, 0, 1000, "MET", "MET [GeV]", "events");
	  _h["MET_phi"] = bookHisto1D("MET_phi", 200, -6.4, 6.4, "MET", "MET [GeV]", "events");

      _h["l_pT"] = bookHisto1D("l_pT", 100, 0, 200, "l_pT", "lepton pT [GeV]", "events");
      _h["y_pT"] = bookHisto1D("y_pT", 100, 0, 200, "y_pT", "photon pT [GeV]", "events");

      _h["lnu_M"] = bookHisto1D("lnu mass", 100, 0, 500, "M_lnu", "lnu invariant mass [GeV]", "events");
      _h["ly_M"] = bookHisto1D("ly mass", 100, 0, 500, "M_ly", "ly invariant mass [GeV]", "events");
      _h["jj_M"] = bookHisto1D("jj mass", 100, 0, 1000, "M_jj", "jj invariant mass [GeV]", "events");
 

      _h["ly_dR"] = bookHisto1D("ly dR", 100, 0, 5, "dR_ly", "ly dR", "events");
      _h["ly_dPhi"] = bookHisto1D("ly dPhi", 100, 0, 3.2, "dPhi_ly", "ly dPhi", "events");
      _h["ly_dEta"] = bookHisto1D("ly dEta", 100, 0, 5, "dEta_ly", "ly dEta", "events");

      _h["jj_dR"] = bookHisto1D("jj dR", 100, 0, 10, "dR_jj", "jj dR", "events");
      _h["jj_dPhi"] = bookHisto1D("jj dPhi", 100, 0, 3.2, "dPhi_jj", "jj dPhi", "events");
      _h["jj_dEta"] = bookHisto1D("jj dEta", 100, 0, 10, "dEta_jj", "jj dEta", "events");


      _h["ly_pT_diff"] = bookHisto1D("ly_pT_diff", 100, 0, 200, "ly_pT_diff", "ly_pT_diff dR", "events");
      _h["y_z3"] = bookHisto1D("y_z3", 100, -2, 2, "y_z3", "y_z3", "events");
      _h["l_z3"] = bookHisto1D("l_z3", 100, -2, 2, "l_z3", "l_z3", "events");

      _h["ly_opening_angle"] = bookHisto1D("ly_opening_angle", 100, 0, 10, "ly_opening_angle", "ly opening angle rest frame", "events");
      _h["ly_costhetastar"] = bookHisto1D("ly_costhetastar", 100, 0, 1, "ly_costhetastar", "ly cos thetastar", "events");

       MSG_INFO("data: weight,yz3,lz3,pTlep,pTgamma,ly_costhetastar,lydr,lydeta,lydphi,mly,jjdr,jjdeta,jjdphi,mjj,MET,MET_phi,lpt,lpz,leta,lphi,ypt,ypz,yeta,yphi,j1pt,j1pz,j1eta,j1phi,j2pt,j2pz,j2eta,j2phi");

    }

    /// Perform the per-event analysis
    void analyze(const Event & event) {

      //this rivet routine selects events with 
      //  (1) a lepton - the type is defined at runtime, by running either myRivetRoutine_EL or myRivetRoutine_MU
      //  (2) a reconstructed W boson from that lepton, with transverse mass mT > 40 GeV and missing transverse energy > 25 GeV
      //       (those cuts are standard to make it a "good" W boson)
      //  (3)  at least one photon with pT > 15 GeV in the fidicual detector volume (|eta| < 2.37)
      //  (4)  and at least two jets > 25 GeV
      
      const double weight = event.weight();
      MSG_DEBUG("event weight: " << weight);
      
      // Apply the WFinder to the event to find exactly 1 W->e or W->mu boson
      const WFinder & w_finder = apply < WFinder > (event, "W_FINDER");
      Particle lepton, neutrino;

      MSG_DEBUG("Found " << w_finder.bosons().size() << " W bosons!");

      if (w_finder.bosons().size() == 1) {
        MSG_DEBUG("W boson found, mT = " << w_finder.mT());
        m_W_events += 1;
        if (!(w_finder.mT() > 40 * GeV && w_finder.constituentNeutrino().Et() > 25.0 * GeV)) vetoEvent;
        lepton = w_finder.constituentLepton();
		neutrino = w_finder.constituentNeutrino();
		MSG_DEBUG("Constitutent lepton, neutrino pT: " << lepton.pT() << ", " << neutrino.pT());
      } else {
        MSG_DEBUG(" No W found passing cuts.");
        // we don't have a W, so aren't going to want to look at this event offline.
        // veto the event (code stops processing and continues onto the next event)
        vetoEvent; 
      }

      //(prints out the line number - a good way of debugging code if it's crashing and you don't know where!)
      // std::cout << __LINE__ << std::endl; 
      
      
      // Get objects
      //vector < DressedLepton > leptons = apply < DressedLeptons > (event, "Leptons").dressedLeptons();
      const Particles & photons = apply < PromptFinalState > (event, "Photons").particlesByPt();
      const Jets jets = apply < FastJets > (event, "Jets").jetsByPt();

      const MissingMomentum& missmom = apply<MissingMomentum>(event, "mm");
      MSG_DEBUG("MET is  :"  << missmom.vectorEt().mod()<< ", neutrino Et: " << neutrino.Et());
      
      if (photons.empty()) vetoEvent; //make sure there are photons
      MSG_DEBUG("number of photons: " << photons.size());

      if (photons.size() < 1) vetoEvent; //make sure event has at least one photon (probably already did that above...)
      MSG_DEBUG("photon pT: " << photons[0].pT()); 
      if(photons[0].pT() < 15 * GeV ) vetoEvent; //make sure photon is >= 15 GeV (should be, because we already defined photons with this in init())

      MSG_DEBUG("lepton pT: " << lepton.pT());
      MSG_DEBUG("lepton photon dR: " << deltaR(photons[0],lepton));

      //make sure the lepton and leading photon are well-separated
      if (deltaR(photons[0], lepton) < 0.7) vetoEvent; 

      if(jets.size() < 2) vetoEvent; //make sure the event has >= 2 jets
      if(jets[1].pT() < 25 * GeV) vetoEvent; //two pt> 25 GeV jets

      //calculate the opening angle in the rest frame and the cos(theta*) between 
      //two particles (here, the leading photon and the lepton in the W)
      
      FourMomentum ph0 = photons[0].momentum();
      FourMomentum lep = lepton.momentum();

      double ly_costhetastar = calcCosThetaStar(ph0,lep);

      Vector3 ly_boost = (photons[0].momentum() + lepton.momentum()).boostVector();
      LorentzTransform centreOfMassyyTrans;
      centreOfMassyyTrans.setBetaVec(-ly_boost);

      FourMomentum ph0_rf = centreOfMassyyTrans.transform(ph0);
      FourMomentum lep_rf = centreOfMassyyTrans.transform(lep);

      double ly_angle_restframe = (ph0_rf).angle(lep_rf);
      
      MSG_DEBUG("angle_rf, costheta* : " << ly_angle_restframe << ", " << ly_costhetastar);
      
	  //calculate all the variables we want to print out!
	  const double met = missmom.vectorMET().mod() / GeV;
	  const double met_phi = missmom.vectorMET().phi();
      const double pTgammal = photons[0].pT() / GeV;
      const double pTlep = lepton.pT() / GeV;
      const double mly = (lepton.momentum() + photons[0].momentum()).mass() / GeV;
      const double avgeta = 0.5 * (photons[0].eta() + lepton.eta());
      const double lydr = deltaR(photons[0], lepton);
      const double lydphi = deltaPhi(photons[0], lepton);
      const double lydeta = deltaEta(photons[0], lepton);
      const double mjj = (jets[0].momentum() + jets[1].momentum()).mass() / GeV;
      const double jjdr = deltaR(jets[0], jets[1]);
      const double jjdphi = deltaPhi(jets[0], jets[1]);
      const double jjdeta = deltaEta(jets[0], jets[1]);
      const double yz3 = photons[0].eta() - avgeta;
      const double lz3 =  lepton.eta() - avgeta;	

       MSG_DEBUG("all calculations done");

       //Here, you print out an info message with all the variables, csv format, in the same order 
       //as the message we put at the start, i.e.:
       //weight,yz3,lz3,pTlep,pTgamma,ly_costhetastar,lydr,lydeta,lydphi,mly,jjdr,jjdeta,jjdphi,mjj,
       //   MET,MET_phi,lpt,lpz,leta,lphi,ypt,ypz,yeta,yphi,j1pt,j1pz,j1eta,j1phi,j2pt,j2pz,j2eta,j2phi

       MSG_INFO("data: " << weight << "," << yz3 << "," << lz3 << "," 
       << pTlep << "," << pTgammal << "," << ly_costhetastar << "," 
       << lydr << "," << lydeta << "," << lydphi << "," << mly << "," 
       << jjdr << "," << jjdeta << "," << jjdphi << "," << mjj  << "," 
       << met << "," << met_phi << "," << pTlep << "," << lepton.pz() / GeV << ","
       << lepton.eta() << "," << lepton.phi() << "," << pTgammal << "," 
       << photons[0].pz() / GeV << "," << photons[0].eta() << "," << photons[0].phi() << ","
       << jets[0].pT() / GeV << "," << jets[0].pz() / GeV << "," << jets[0].eta() << "," << jets[0].phi() << "," 
       << jets[1].pT() / GeV << "," << jets[1].pz() / GeV << "," << jets[1].eta() << "," << jets[1].phi() );


      //fill some histograms, if you want (or can recreate them later from the csv file)
      //if you wanted to apply cuts before filling histograms, you could also do it now
      //because we have already printed out the event data for the csv file
      _h["MET"]->fill(met, weight);
      _h["MET_phi"]->fill(met_phi, weight);
      _h["y_z3"]->fill(yz3, weight);
      _h["l_z3"]->fill(lz3, weight);
      MSG_DEBUG("done z3");
	
      _h["l_pT"]->fill(pTlep, weight);
      _h["y_pT"]->fill(pTgammal, weight);
      _h["ly_pT_diff"]->fill(pTlep - pTgammal, weight);
      _h["ly_opening_angle"]->fill(ly_angle_restframe, weight);
      _h["ly_costhetastar"]->fill(ly_costhetastar, weight);

      _h["ly_dR"]->fill(lydr, weight);
      _h["ly_dEta"]->fill(lydeta, weight);
      _h["ly_dPhi"]->fill(lydphi, weight);
      _h["ly_M"]->fill(mly, weight);

      _h["jj_dR"]->fill(jjdr, weight);
      _h["jj_dEta"]->fill(jjdeta, weight);
      _h["jj_dPhi"]->fill(jjdphi, weight);
      _h["jj_M"]->fill(mjj, weight);

       MSG_DEBUG("Filled some histograms");
       

    }

    /// Normalise histograms etc., after the run
    void finalize() {
      const double sf = crossSection() / femtobarn / sumOfWeights();
      std::cout << "******* COUNTS *********" << std::endl;
      std::cout << "  W events : " << m_W_events << std::endl;
      std::cout << " sum of weights: " << sumOfWeights() << std::endl;
      std::cout << " scale factor: " << sf << std::endl;
      std::cout << "************************" << std::endl;
      bool doScale = false;
      if(doScale){ 
        for (const auto & kv: _h) scale(kv.second, sf);
        std::cout << "Scaled all the histograms" << std::endl;
      }
    }


      double calcChi(const FourMomentum& t1, const FourMomentum& t2) {
        double ystar = 0.5 * (t1.rapidity()-t2.rapidity());
        double chi = exp( 2 * abs(ystar));
        return chi;
      }

      double calcCosThetaStar(const FourMomentum& t1, const FourMomentum& t2) {
        FourMomentum ttbar = t1 + t2;
        LorentzTransform centreOfMassTrans;
        ttbar.setX(0);
        ttbar.setY(0);
        centreOfMassTrans.setBetaVec( -ttbar.boostVector() );
        FourMomentum t1_star = centreOfMassTrans.transform(t1);
        double cosThetaStar = t1_star.pz()/t1_star.p3().mod();
        return cosThetaStar;
      }
      
    //@}

    protected:

      // Data members like post-cuts event weight counters go here
      size_t _mode;

    private:

      /// Histograms
      map < string, Histo1DPtr > _h;
    double m_W_events;

  };

  struct myRivetRoutine_EL: public myRivetRoutine {
    myRivetRoutine_EL(): myRivetRoutine("myRivetRoutine_EL", 0) {}
  };

  struct myRivetRoutine_MU: public myRivetRoutine {
    myRivetRoutine_MU(): myRivetRoutine("myRivetRoutine_MU", 1) {}
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(myRivetRoutine);
  DECLARE_RIVET_PLUGIN(myRivetRoutine_EL);
  DECLARE_RIVET_PLUGIN(myRivetRoutine_MU);

}
